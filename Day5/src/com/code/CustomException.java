package com.code;

import java.util.Scanner;

/***
 * program to get name and age of player
 * 
 * @author gopiraj.sahithi
 *
 */
public class CustomException {

	public static void main(String[] args) {
		String name;
		int age;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter name of the cricketer:");
		name = sc.next();
		System.out.println("Enter age :");
		age = sc.nextInt();
		try {
			if (age < 16)
				System.out.println("InvalidAgeRangeException");
			else {
				System.out.println("Name of the Cricketer : " + name);
				System.out.println(" Age of the Cricketer : " + age);
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		sc.close();
	}
}
