package com.code;

import java.util.InputMismatchException;
import java.util.Scanner;

/***
 * example on try catch block
 * 
 * @author gopiraj.sahithi
 *
 */
public class HandleException {

	public static void main(String[] args) {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Enter input1");
			int input1 = scan.nextInt();
			System.out.println("Enter input2");
			int input2 = scan.nextInt();
			scan.close();
			System.out.println(divideInput(input1, input2));
		} catch (ArithmeticException ae) {
			System.err.print("java.lang.ArithemeticException");
		} catch (InputMismatchException ime) {
			System.err.print("java.util.InputMismatchException");
		}

	}

	private static int divideInput(int input1, int input2) {
		int division;
		division = input1 / input2;
		return division;
	}
}