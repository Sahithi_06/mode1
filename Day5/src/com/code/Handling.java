package com.code;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/***
 * File handling exception
 * 
 * @author gopiraj.sahithi
 *
 */
public class Handling {
	public void fileHandling() {
		try {
			InputStreamReader reader = new InputStreamReader(new FileInputStream(
					"G:\\SeleniumTrainedByJithendra\\practicals\\Eclipse_JavaPracticals\\Day5_Assignment\\src\\com\\hcl\\model\\MyFile.txt"));
			BufferedReader bufferReader = new BufferedReader(reader);
			String line;

			while ((line = bufferReader.readLine()) != null) {
				System.out.println(line);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
