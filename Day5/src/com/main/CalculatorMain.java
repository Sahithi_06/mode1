package com.main;

import java.util.Scanner;
import com.code.Calculator;

/***
 * power of a number by implementing calculator
 * 
 * @author gopiraj.sahithi
 *
 */
public class CalculatorMain {
	public static final Calculator calculator = new Calculator();
	public static final Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		while (sc.hasNextInt()) {
			int n = sc.nextInt();
			int p = sc.nextInt();

			try {
				System.out.println(Calculator.power(n, p));
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}
}
