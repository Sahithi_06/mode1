package com.main;

import com.code.Handling;

/***
 * File handling exception
 * 
 * @author gopiraj.sahithi
 *
 */
public class FileHandlingMain {

	public static void main(String[] args) {
		Handling handle = new Handling();
		handle.fileHandling();
	}

}
