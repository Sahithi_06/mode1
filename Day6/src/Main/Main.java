package Main;

import java.text.SimpleDateFormat;
import java.util.Date;

import code.UserMainCode;

public class Main {

	public static void main(String[] args) {
		try {
			UserMainCode t1 = new UserMainCode();

			System.out.println("Name of t1:" + t1.getName());

			t1.start();

			t1.setName("MyThread");
			System.out.println("After changing name of t1:" + t1.getName());
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss");
			System.out.println("time : " + sdf.format(date));

			Thread.sleep(10000);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("kk:mm:ss");
			System.out.println("time after sleep : " + sdf1.format(date1));
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

	}

}
