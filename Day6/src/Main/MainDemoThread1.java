package Main;

import code.DemoThread1;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class MainDemoThread1 {

	public static void main(String[] args) {
		DemoThread1 obj1 = new DemoThread1();

		DemoThread1 obj2 = new DemoThread1();

		DemoThread1 obj3 = new DemoThread1();
		obj1.run();
		obj2.run();
		obj3.run();
	}

	public void DemoThread1() {

		Thread thread = new Thread();
		System.out.println("Name of thread:" + thread.getName());
		thread.start();

	}

}
