package Main;

import java.text.SimpleDateFormat;
import java.util.Date;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class UserMainCode1 {

	public static void main(String[] args) throws InterruptedException {
		Thread thread = new Thread();
		System.out.println("Name of t1:" + thread.getName());

		thread.start();

		thread.setName("MyThread");
		System.out.println("After changing name of thread:" + thread.getName());
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss");
		System.out.println("time : " + sdf.format(date));

		Thread.sleep(10000);
		Date date1 = new Date();
		SimpleDateFormat sdf1 = new SimpleDateFormat("kk:mm:ss");
		System.out.println("time after sleep : " + sdf1.format(date1));

	}

}
