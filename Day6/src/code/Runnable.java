package code;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public interface Runnable {
	public void run();
}
