package com.code;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class CompareTwoDates {
	public static String findOldDate(String date1, String date2) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		Date date3 = null;

		try {
			date = dateFormat.parse(date1);
			date3 = dateFormat.parse(date2);
		} catch (ParseException e) {

			e.printStackTrace();
		}

		if (date.compareTo(date3) > 0) {
			String format = new SimpleDateFormat("MM/dd/yyyy").format(date);
			return format;
		}

		else if (date.compareTo(date3) < 0) {
			String format1 = new SimpleDateFormat("MM/dd/yyyy").format(date);
			return format1;
		}

		else if (date.compareTo(date3) == 0) {
			return "both dates are same";
		}
		return null;
	}
}
