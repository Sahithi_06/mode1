package com.code;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class ConvertDate {
	public static String convertDateFormat(String inputString) {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = dateFormat.parse(inputString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
		inputString = dateFormat1.format(date);
		// System.out.println(" Converted date is : "+ inputString);
		return inputString;
	}
}
