package com.code;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class GetNumberOfDays {
	public static int getNumberOfDays(int input1, int input2) {

		switch (input2) {
		case 0:
			return 31;
		case 2:
			return 31;
		case 3:
			return 30;
		case 4:
			return 31;
		case 5:
			return 30;
		case 6:
			return 31;
		case 7:
			return 31;
		case 8:
			return 30;
		case 9:
			return 31;
		case 10:
			return 30;
		case 11:
			return 31;
		default:

			if (input1 % 400 == 0) {
				if (input2 == 1)
					return 29;
				else
					return 28;
			} else if (input1 % 4 == 0 && input1 % 100 != 0) {
				if (input2 == 1)
					return 29;
				else
					return 28;
			} else
				return 28;
		}

	}
}
