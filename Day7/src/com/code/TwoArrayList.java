package com.code;

import java.util.ArrayList;
import java.util.Collections;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class TwoArrayList {
	public static ArrayList<Integer> sortMergedArrayList(ArrayList<Integer> arrayList, ArrayList<Integer> arrayList2) {

		arrayList.addAll(arrayList2);
		Collections.sort(arrayList);

		ArrayList<Integer> arrayList3 = new ArrayList<Integer>(10);
		arrayList3.add(arrayList.get(2));
		arrayList3.add(arrayList.get(6));
		arrayList3.add(arrayList.get(8));
		return arrayList3;
	}
}
