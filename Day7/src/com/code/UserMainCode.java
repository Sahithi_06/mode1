package com.code;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class UserMainCode {
	static char firstLetter;
	static char lastLetter;

	public static int checkCharacters(String inputString) {

		int length = inputString.length();
		for (int i = 0; i < length; i++) {
			if (i == 0)
				firstLetter = inputString.charAt(i);
			if (i == length - 1)
				lastLetter = inputString.charAt(length - 1);
		}

		return (firstLetter == lastLetter) ? 1 : -1;

	}
}
