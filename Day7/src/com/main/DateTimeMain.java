package com.main;

import java.util.Date;
import java.text.SimpleDateFormat;
import com.code.GetDteTime;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class DateTimeMain {

	public static void main(String[] args) {
		Date date = new Date();
		GetDteTime.getDate(date);
		GetDteTime.getTime(date);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
		simpleDateFormat.format(date);

	}

}
