package com.main;

import com.code.UserMainCode;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class FirstAndLastMain {

	public static void main(String[] args) {
		int value = UserMainCode.checkCharacters("the picture was great");
		System.out.println((value > 0) ? "valid" : "invalid");
	}

}
