package com.main;

import com.code.GetNumberOfDays;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class GetNumberOfDaysMain {

	public static void main(String[] args) {
		System.out.println(GetNumberOfDays.getNumberOfDays(2000, 1));
		System.out.println(GetNumberOfDays.getNumberOfDays(2001, 1));
		System.out.println(GetNumberOfDays.getNumberOfDays(2000, 0));

	}

}
