package com.main;

import java.util.ArrayList;
import java.util.Scanner;

import com.code.TwoArrayList;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class Main {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		ArrayList<Integer> arrayList = new ArrayList<Integer>(5);
		ArrayList<Integer> arrayList2 = new ArrayList<Integer>(5);
		ArrayList<Integer> arrayList3 = new ArrayList<Integer>(3);

		for (int i = 0; i < arrayList.size(); i++) {
			arrayList.add(scanner.nextInt());
		}

		for (int i = 0; i < arrayList2.size(); i++) {
			arrayList2.add(scanner.nextInt());
		}

		arrayList3 = TwoArrayList.sortMergedArrayList(arrayList, arrayList2);
		for (int k = 0; k < arrayList3.size(); k++) {
			System.out.println(arrayList3.get(k));
		}

	}

}
