package com.main;

import java.util.Calendar;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class MaximumValueMain {

	public static void main(String[] args) {

		Calendar calender = Calendar.getInstance();

		System.out.println(calender.getActualMaximum(Calendar.YEAR));
		System.out.println(calender.getActualMaximum(Calendar.MONTH));
		System.out.println(calender.getActualMaximum(Calendar.WEEK_OF_MONTH));
		System.out.println(calender.getActualMaximum(Calendar.DATE));

	}

}
