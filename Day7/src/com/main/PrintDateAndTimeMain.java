package com.main;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class PrintDateAndTimeMain {

	public static void main(String[] args) {
		Date date = new Date();
		System.out.println("Date= " + date);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
		System.out.println("simple date format is " + simpleDateFormat.format(date));
		Calendar calender = Calendar.getInstance();
		System.out.println("the current date and time is : " + calender.getTime());
	}

}
