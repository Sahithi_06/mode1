package com.main;

import java.util.Calendar;
import java.util.Date;

import com.code.PrintYear;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class PrintYearMain {

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Date date = new Date();

		Calendar calender = Calendar.getInstance();
		PrintYear.getYear(calender);
		PrintYear.getMonth(calender);
		PrintYear.getDay(calender);
		PrintYear.getHour(calender);
		PrintYear.getMinute(calender);
	}

}
