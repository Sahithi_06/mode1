package com.main;

import java.net.UnknownHostException;

import com.code.IPAddress;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class ValidateMain {

	public static void main(String[] args) {
		try {
			int a = IPAddress.ipValidate("132.145.184.210");
			System.out.println((a == 1) ? "valid" : "invalid");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
