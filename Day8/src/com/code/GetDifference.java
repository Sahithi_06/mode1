package com.code;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class GetDifference {
	public static int getDifference(String date1, String date2) throws ParseException {

		Date date = new SimpleDateFormat("dd/MM/YYYY").parse(date1);
		Date date3 = new SimpleDateFormat("dd/MM/YYYY").parse(date2);

		Calendar cal = Calendar.getInstance();

		cal.setTime(date);
		cal.setTime(date3);

		@SuppressWarnings("deprecation")
		int day1 = date.getDate();

		@SuppressWarnings("deprecation")
		int day2 = date3.getDate();

		System.out.println(day1);
		System.out.println(day2);
		if (day1 > day2) {
			return day1 - day2;
		}

		else
			return day2 - day1;

	}
}
