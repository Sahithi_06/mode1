package com.code;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class HelloHcl {
	public void hcl() {
		try {
			InputStreamReader reader = new InputStreamReader(new FileInputStream("G:\\MyFile.txt"));
			BufferedReader bufferReader = new BufferedReader(reader);

			if (bufferReader.readLine() == "Hoshitha/n") {
				System.out.println("Hello HCL");
			} else {
				System.out.println("input does not match");
			}
			reader.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

	}
}
