package com.code;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class LongestWord {
	static String longestWord = " ";
	static String[] words;

	public static String findLongestWord() throws FileNotFoundException {
		String longest_word = "";
		String current;
		@SuppressWarnings("resource")
		Scanner input = new Scanner(new File("G:\\SeleniumTrainedByJithendra\\notes\\longestWord.txt"));
		while (input.hasNext()) {
			current = input.next();
			if (current.length() > longest_word.length()) {
				longest_word = current;
			}
		}
		System.out.println("\n" + longest_word + "\n");
		return longest_word;
	}
}