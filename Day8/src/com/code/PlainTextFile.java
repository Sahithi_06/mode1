package com.code;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class PlainTextFile {
	String line;

	public void fileHandling() {

		File createFile = new File("G:\\Day5_assignment.txt");
		try {

			@SuppressWarnings("resource")
			FileWriter fileWriter = new FileWriter(createFile);
			fileWriter.write("write this into the file");
			// fileWriter.close();

			@SuppressWarnings("resource")
			BufferedReader bufferReader = new BufferedReader(new FileReader(createFile));
			line = bufferReader.readLine();
			while (line != null) {
				System.out.println(line);
				line = bufferReader.readLine();
			}
		} catch (IOException ioe) {
			System.out.println(ioe);
		}

		System.out.println("success..");

	}

	public void fileHandling1() {
		// TODO Auto-generated method stub

	}
}
