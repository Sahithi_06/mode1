package com.main;

import java.text.ParseException;

import com.code.GetDifference;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class GetDifferenceMain {

	public static void main(String[] args) {
		try {
			System.out.println(GetDifference.getDifference("04/03/2012", "12/03/2012"));
		} catch (ParseException e) {

			e.printStackTrace();
		}
	}

}
