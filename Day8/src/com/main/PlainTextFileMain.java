package com.main;

import com.code.PlainTextFile;

/***
 * 
 * @author gopiraj.sahithi
 *
 */
public class PlainTextFileMain {

	public static void main(String[] args) {
		PlainTextFile textFile = new PlainTextFile();
		textFile.fileHandling();
	}

}
